package ru.mail.park.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.kohsuke.randname.RandomNameGenerator;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final RandomNameGenerator GENERATOR = new RandomNameGenerator();

    private RecyclerView recyclerView;

    private boolean isGrid2 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final DataSource dataSource = new DataSource();
        recyclerView = (RecyclerView) findViewById(R.id.container);
        recyclerView.setAdapter(new RecyclerView.Adapter() {
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                if (viewType == 1) {
                    return new ItemViewHolder(
                            getLayoutInflater().inflate(R.layout.test2, parent, false)
                    );
                }
                return new ItemViewHolder(
                        getLayoutInflater().inflate(R.layout.test, parent, false)
                );
            }

            @Override
            public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
                Item item = dataSource.getItem(position);
                ItemViewHolder h = (ItemViewHolder) holder;
                h.bind(item);
                if (isGrid2) {

                    double span = 1;

                    int p = position % 15;

                    if (p == 0 || p == 5 || p == 10) {
                        span = 6;
                    } else if (p == 1 || p == 6 || p == 11) {
                        span = 3.7;
                    } else if (p == 4 || p == 9 || p == 14) {
                        span = 2;
                    } else if (p == 2 || p == 7 || p == 12) {
                        span = 1.5;
                    } else if (p == 3 || p == 8 || p == 13) {
                        span = 5;
                    }

                    h.setHeight(containerHeight(MainActivity.this, span));
                }
            }

            @Override
            public int getItemCount() {
                return dataSource.getCount();
            }

            @Override
            public int getItemViewType(int position) {
                return position == 1 ? 1 : 2;
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataSource.addItem(generateItem());
            }
        });

        findViewById(R.id.remove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataSource.removeFirst();
            }
        });

        findViewById(R.id.vertical).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                isGrid2 = false;
            }
        });

        findViewById(R.id.horizontal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false));
                isGrid2 = false;
            }
        });

        findViewById(R.id.grid).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 3));
                isGrid2 = false;
            }
        });

        findViewById(R.id.grid3Button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GridLayoutManager glm = new GridLayoutManager(MainActivity.this, 3);
                glm.setSpanSizeLookup(new Grid3SpanSizeLookup());
                recyclerView.setLayoutManager(glm);
                isGrid2 = false;
            }
        });

        findViewById(R.id.grid2Button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaggeredGridLayoutManager sglm = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(sglm);
                isGrid2 = true;
            }
        });
    }

    private static int containerHeight(MainActivity ma, double spanCount) {
        DisplayMetrics dm = new DisplayMetrics();
        ma.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return (int) (dm.heightPixels * spanCount / 24.0);
    }

    private Item generateItem() {
        return new Item(GENERATOR.next(), GENERATOR.next());
    }

    private static class Item {

            private final String text1;
            private final String text2;

        Item(String text1, String text2) {
            this.text1 = text1;
            this.text2 = text2;
        }

        public String getText1() {
            return text1;
        }

        public String getText2() {
            return text2;
        }
    }

    private class DataSource {

        private final List<Item> items = new ArrayList<>();

        public int getCount() {
            return items.size();
        }

        public Item getItem(int position) {
            return items.get(position);
        }

        public void addItem(Item item) {
            items.add(item);
            recyclerView.getAdapter().notifyItemInserted(items.size() - 1);
        }

        public void removeFirst() {
            if (!items.isEmpty()) {
                items.remove(0);
                recyclerView.getAdapter().notifyItemRemoved(0);
            }
        }

    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {

        private final TextView text1;
        private final TextView text2;
        private final View itemView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            this.text1 = (TextView) itemView.findViewById(R.id.text1);
            this.text2 = (TextView) itemView.findViewById(R.id.text2);
            this.itemView = itemView;
        }

        public void bind(Item item) {
            text1.setText(item.getText1());
            text2.setText(item.getText2());
            itemView.setMinimumHeight(0);
        }

        public void setHeight(int height) {
            itemView.setMinimumHeight(height);
        }

    }

    private class Grid3SpanSizeLookup extends GridLayoutManager.SpanSizeLookup {

        @Override
        public int getSpanSize(int position) {
            int m = position % 7;
            if (m == 0) {
                return 1;
            } else if (m == 2 || m == 3 || m == 4) {
                return 1;
            } else {
                return 3;
            }
        }
    }

}
